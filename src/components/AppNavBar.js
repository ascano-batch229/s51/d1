import NavBar from 'react-bootstrap/NavBar';
import Nav from 'react-bootstrap/Nav';

export default function	AppNavBar(){
	return (
		<NavBar bg="light" expand="lg" className="p-3">
			<NavBar.Brand href="#home"> Zuitt </NavBar.Brand>
			<NavBar.Toggle aria-controls="basic-navbar-nav"/>
			<NavBar.Collapse id="basic-navbar-nav">	
				<Nav className="ml-auto">
					<Nav.Link href="#home"> Home </Nav.Link>
					<Nav.Link href="#courses"> Courses</Nav.Link>

				</Nav>

			</NavBar.Collapse>
		</NavBar>


		)
}